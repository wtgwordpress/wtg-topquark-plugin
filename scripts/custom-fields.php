<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

$ajax_nonce = wp_create_nonce( "wtg-topq" );

?>
	(function($) {
		
		var getSpeakerIdFromURL = function() {
			return $('[name=id]').val();
		}
		
		var postData = function(elem) {
			
			var data = {
				value: $(elem).val(),
				id: getSpeakerIdFromURL()
			}
			
			$.ajax({
				url: ajaxurl,
				data: {
					action: 'Add'+$(elem).attr('name'),
					data: data,
					security: '<?php echo $ajax_nonce; ?>'
				},
				type: 'post',
				success: function(res) {
					console.log(res)
				},
				error: function(res) {
					console.log(res)
				}
			})
			
		}
		
		var GetData = function(elem) {
			var data = {
				id: getSpeakerIdFromURL()
			}
			
			$.ajax({
				url: ajaxurl,
				data: {
					action: 'Get'+$(elem).attr('name'),
					data: data
				},
				type: 'get',
				success: function(res) {
					if (res.status === 'success') {
						$(elem).val(res.data);
					}
				},
				error: function(res) {
					// @TODO show some message
				}
			});
		}
		
		$(document).ready(function() {
			
			if ( $("#ArtistLastName").length === 0)
				return;
			
			if (typeof window.location.search === 'undefined'
				|| (window.location.search.indexOf('id=') === -1
				&& window.location.search.indexOf('package=FestivalApp') === -1))
				return;
			
			var InputField = function(name, label) {
			return  ''
			+ '<tr>'
			  + '<th valign="top" align="left" width="20%">'+label+':</th>'
			  + '<td valign="top" align="left">'
				+ '<input type="text" name="'+name+'" id="'+name+'" size="50" value="">'
			  + '</td>'
			 + '</tr>';
			}

			var AddField = function( html, InputName, afterElemId ) {
				if ( $('[name='+InputName+']').length < 1)
					$(afterElemId).closest('tr').after( html );
				
				GetData('#'+InputName);
				
				var timer;
				$('#'+InputName).on('keyup', function() {
					var base = this;
					timer && clearTimeout(timer);
					timer = setTimeout(function() {
						console.log("here", $(base).val());
						postData( base );
					},650)
				});
			}
			
			AddField( InputField('SpeakerEmail', 'Speaker Email (not published)'), 'SpeakerEmail', '#ArtistLastName');
			AddField( InputField('SpeakerIndustrySector', 'Industry Sector'), 'SpeakerIndustrySector', '#ArtistLastName');
			AddField( InputField('SpeakerCountryOfOrigin', 'Country of Origin'), 'SpeakerCountryOfOrigin', '#ArtistLastName');
			
		});
		
	})(jQuery);
