<?php
/**
 * Plugin Name: WTG TopQuark
 * Plugin URI: https://bitbucket.org/wtgwordpress/wtg-topquark-plugin
 * Description: Adds custom features to TopQuark
 * Version: 1.0.0
 * Author: Matty
 * Author URI: http://wtgevents.com
 * Requires at least: 4.0.0
 * Tested up to: 4.0.0
 *
 * Text Domain: starter-plugin
 * Domain Path: /languages/
 *
 * @package WTG TopQuark
 * @category Core
 * @author Matty
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function Starter_Plugin() {
	return WTG_TopQuark_Plugin::instance();
}

add_action( 'plugins_loaded', 'Starter_Plugin' );

final class WTG_TopQuark_Plugin {

	private static $_instance = null;

	public $token;

	public $version;

	public $plugin_url;

	public $plugin_path;

	public $admin;

	public $settings;
	
	public $post_types = array();
	
	public $controllers = array();

	public function __construct () {
		$this->name				= 'WTG Topquark Plugin';
		$this->token 			= 'wtg-topquark-plugin';
		$this->plugin_url 		= plugin_dir_url( __FILE__ );
		$this->plugin_path 		= plugin_dir_path( __FILE__ );
		$this->version 			= '0.0.1';
		
		require 'updater/plugin-update-checker.php';
		$myUpdateChecker = PucFactory::buildUpdateChecker(
			'https://s3-eu-west-1.amazonaws.com/wtgwpstore/wtgplugin/wtg-topquark/metadata.json?v='.$this->version,
			__FILE__
		);

		// Admin - Start
		require_once( 'classes/class-wtg-topquark-plugin-settings.php' );
			$this->settings = WTG_TopQuark_Plugin_Settings::instance();

		if ( is_admin() ) {
			require_once( 'classes/class-wtg-topquark-admin.php' );
			$this->admin = WTG_TopQuark_Plugin_Admin::instance();
		}
		// Admin - End

		add_action( 'init', array( $this, 'CustomFieldAjaxController'));
		
		require_once( 'classes/class-wtg-topquark-plugin-post-type.php' );

		$this->post_types['wtgspeakers'] = new WTG_TopQuark_Plugin_Post_Type( 'wtgspeakers', __( 'WTG Speaker', 'wtg-topquark-plugin' ), __( 'WTG Speakers', 'wtg-topquark-plugin' ), array( 'menu_icon' => 'dashicons-carrot' ) );

		register_activation_hook( __FILE__, array( $this, 'install' ) );

		add_action( 'init', array( $this, 'load_plugin_textdomain' ) );
		
	}

	public static function instance () {
		if ( is_null( self::$_instance ) )
			self::$_instance = new self();
		return self::$_instance;
	}

	public function load_plugin_textdomain() {
		load_plugin_textdomain( 'starter-plugin', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
	}


	public function __clone () {
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?' ), '0.0.1' );
	} 
	
	public function __wakeup () {
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?' ), '0.0.1' );
	}


	public function install () {
		$this->_log_version_number();
	}

	private function _log_version_number () {
		update_option( $this->token . '-version', $this->version );
	}
	
	function activate_au() {
		require_once ( 'updater/wp_autoupdate.php' );
		$plugin_current_version = $this->version;
		$plugin_remote_path = 'https://bitbucket.org/wtgwordpress/wtg-topquark-plugin/src/updater/metadata.json?version='.$this->version;	
		$plugin_slug = plugin_basename( __FILE__ );
		$license_user = '';
		$license_key = '';
		new WP_AutoUpdate ( $plugin_current_version, $plugin_remote_path, $plugin_slug, $license_user, $license_key );	
	}
	
	public function CustomFieldAjaxController() {
		require_once ( dirname(__FILE__) . '/controllers/wtgspeakersController.php' );
		$this->controllers['wtgspeakers'] = new wtgSpeakersController();
	}
	
}


?>
