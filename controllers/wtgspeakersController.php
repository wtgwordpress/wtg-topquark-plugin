<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class wtgspeakersController {
	
	public $post_type;
	
	public function __construct() {
		
		$this->post_type = 'wtgspeakers';
		
		
		add_action( 'wp_ajax_GetSpeakerCountryOfOrigin', array($this, 'GetSpeakerCountryOfOrigin' ));
		add_action( 'wp_ajax_GetSpeakerIndustrySector', array($this, 'GetSpeakerIndustrySector' ));
		add_action( 'wp_ajax_GetSpeakerEmail', array($this, 'GetSpeakerEmail' ));
		
		add_action( 'wp_ajax_AddSpeakerCountryOfOrigin', array($this, 'AddSpeakerCountryOfOrigin' ));
		add_action( 'wp_ajax_AddSpeakerIndustrySector', array($this, 'AddSpeakerIndustrySector' ));
		add_action( 'wp_ajax_AddSpeakerEmail', array($this, 'AddSpeakerEmail' ));
	}
	
	public function AddSpeakerCountryOfOrigin() {
		check_ajax_referer( 'wtg-topq', 'security' );
		
		if (!$this->isReqestedMethodByPost($_SERVER['REQUEST_METHOD']))
			return status_header(404);
		
		if (!$this->postDataIsValid($_POST))
			return status_header(400);
		
		$post = $this->getSpeakerPost($_POST);
	
		$MetaKey = 'SpeakerCountryOfOrigin';
		update_post_meta($post->ID, $MetaKey, $_POST['data']['value']);
		
		$response = (object) array(
			'status' => 'success',
			'message' => 'Speaker country added'
		);
		
		wp_send_json( $response );
	}
	
	public function GetSpeakerCountryOfOrigin() {
		
		if (!$this->isReqestedMethodByGet($_SERVER['REQUEST_METHOD']))
			return status_header(404);
		
		if (!$this->getDataIsValid($_GET))
			return status_header(400);
		
		$post = $this->getSpeakerPost($_GET);
		
		$MetaKey = 'SpeakerCountryOfOrigin';

		$value = get_post_meta($post->ID, $MetaKey, true);
		
		$response = (object) array(
			'status' => 'success',
			'data' => $value
		);
		
		wp_send_json( $response );
		
	}
	
	public function AddSpeakerIndustrySector() {
		check_ajax_referer( 'wtg-topq', 'security' );
		
		if (!$this->isReqestedMethodByPost($_SERVER['REQUEST_METHOD']))
			return status_header(404);
		
		if (!$this->postDataIsValid($_POST))
			return status_header(400);
		
		$post = $this->getSpeakerPost($_POST);
	
		$MetaKey = 'SpeakerIndustrySector';
		update_post_meta($post->ID, $MetaKey, $_POST['data']['value']);
		
		$response = (object) array(
			'status' => 'success',
			'message' => 'Speaker industry added'
		);
		
		wp_send_json( $response );
	}
	
	public function GetSpeakerIndustrySector() {
		if (!$this->isReqestedMethodByGet($_SERVER['REQUEST_METHOD']))
			return status_header(404);
		
		if (!$this->getDataIsValid($_GET))
			return status_header(400);
		
		$post = $this->getSpeakerPost($_GET);
		
		$MetaKey = 'SpeakerIndustrySector';

		$value = get_post_meta($post->ID, $MetaKey, true);
		
		$response = (object) array(
			'status' => 'success',
			'data' => $value
		);
		
		wp_send_json( $response );
	}
	
	public function AddSpeakerEmail() {
		check_ajax_referer( 'wtg-topq', 'security' );
		
		if (!$this->isReqestedMethodByPost($_SERVER['REQUEST_METHOD']))
			return status_header(404);
		
		if (!$this->postDataIsValid($_POST))
			return status_header(400);
		
		$post = $this->getSpeakerPost($_POST);
	
		$MetaKey = 'SpeakerEmail';
		update_post_meta($post->ID, $MetaKey, $_POST['data']['value']);
		
		$response = (object) array(
			'status' => 'success',
			'message' => 'Speaker email added'
		);
		
		wp_send_json( $response );
	}
	
	public function GetSpeakerEmail() {
		if (!$this->isReqestedMethodByGet($_SERVER['REQUEST_METHOD']))
			return status_header(404);
		
		if (!$this->getDataIsValid($_GET))
			return status_header(400);
		
		$post = $this->getSpeakerPost($_GET);
		
		$MetaKey = 'SpeakerEmail';

		$value = get_post_meta($post->ID, $MetaKey, true);
		
		$response = (object) array(
			'status' => 'success',
			'data' => $value
		);
		
		wp_send_json( $response );
	}
	
	public function isReqestedMethodByPost($request_method) {
		return (strtolower($request_method) === 'post');
	}
	
	public function isReqestedMethodByGet($request_method) {
		return (strtolower($request_method) === 'get');
	}
	
	public function postDataIsValid($_post) {
		$valid = true;
		
		if (is_null($_post))
			$valid = false;
		
		if (is_null($_post['data']))
			$valid = false;
		
		if (is_null($_post['data']['id']))
			$valid = false;
		
		if (!is_numeric($_post['data']['id']))
			$valid = false;
		
		if (is_null($_post['data']['value']))
			$valid = false;
		
		return $valid;
	}
	
	public function getDataIsValid($_get) {
		$valid = true;
		
		if (is_null($_get))
			$valid = false;
		
		if (is_null($_get['data']))
			$valid = false;
		
		if (is_null($_get['data']['id']))
			$valid = false;
		
		if (!is_numeric($_get['data']['id']))
			$valid = false;
		
		return $valid;
	}
	
	public function getSpeakerPost($_request) {
		$args = array(
			'post_type' => $this->post_type,
			'name' => 'speaker-'.$_request['data']['id']
		);
		
		$query = new WP_Query($args);

		if (count($query->posts) === 0) {
			$insertpost = array(
				'post_type' => $this->post_type,
				'post_name' => 'speaker-'.$_request['data']['id'],
				'post_title' => 'speaker-'.$_request['data']['id'],
				'post_status' => 'publish'
			);
			$post = (object) $insertpost;
			$post->ID = wp_insert_post($insertpost);
		} else {
			$post = $query->posts[0];
		}
		
		return $post;
		
	}
	
}


?>