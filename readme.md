WTG TopQuark Plugin
--------------------

This adds features onto the existing, and outdated plugin.

Deploy Notes
------------

* To deploy, update the file versions in wtg-topquark.php.
* Update the metadata.json version number
* Zip the file and name it wtg-topquark.zip
* Upload the metadata.json and the wtg-topquark.zip to the S3 bucket inside wtgwpstore/wtgplugin/wtg-topquark - You may have to delete the 2 current files
* Make the 2 upload files public
* Version control your deployment.


Changelog
-----------

** Unreleased **
* XXXX-XX-XX

* Initial release. Woo!