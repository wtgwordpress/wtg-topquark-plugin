<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Starter_Plugin_Admin Class
 *
 * @class Starter_Plugin_Admin
 * @version	1.0.0
 * @since 1.0.0
 * @package	Starter_Plugin
 * @author Jeffikus
 */
final class WTG_TopQuark_Plugin_Admin {
	/**
	 * Starter_Plugin_Admin The single instance of Starter_Plugin_Admin.
	 * @var 	object
	 * @access  private
	 * @since 	1.0.0
	 */
	private static $_instance = null;

	/**
	 * The string containing the dynamically generated hook token.
	 * @var     string
	 * @access  private
	 * @since   1.0.0
	 */
	private $_hook;

	/**
	 * Constructor function.
	 * @access  public
	 * @since   1.0.0
	 */
	public function __construct () {

		add_action( 'admin_head', array($this,'my_scripts' ));
	} // End __construct()

	/**
	 * Main Starter_Plugin_Admin Instance
	 *
	 * Ensures only one instance of Starter_Plugin_Admin is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @static
	 * @return Main Starter_Plugin_Admin instance
	 */
	public static function instance () {
		if ( is_null( self::$_instance ) )
			self::$_instance = new self();
		return self::$_instance;
	} // End instance()

	function my_scripts() {
		$screen = get_current_screen();
		if ( in_array( $screen->id, array('toplevel_page_topquark'))) {

			$info = '<script>';
		
			ob_start();
			include dirname(dirname(__FILE__)) . '/scripts/custom-fields.php';
			$info .= ob_get_clean();
			$info .= '</script>';
			
			
			echo $info;
		
			return;
		}
	}
	
} // End Class
