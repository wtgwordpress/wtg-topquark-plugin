<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly.

class WTG_TopQuark_Plugin_Post_Type {
	/**
	 * The post type token.
	 * @access public
	 * @since  1.0.0
	 * @var    string
	 */
	public $post_type;

	/**
	 * The post type singular label.
	 * @access public
	 * @since  1.0.0
	 * @var    string
	 */
	public $singular;

	/**
	 * The post type plural label.
	 * @access public
	 * @since  1.0.0
	 * @var    string
	 */
	public $plural;

	/**
	 * The post type args.
	 * @access public
	 * @since  1.0.0
	 * @var    array
	 */
	public $args;
	
	public function __construct( $post_type = 'thing', $singular = '', $plural = '', $args = array()) {
		$this->post_type = $post_type;
		$this->singular = $singular;
		$this->plural = $plural;
		$this->args = $args;

		add_action( 'init', array( $this, 'register_post_type' ) );

		if ( is_admin() ) {
			global $pagenow;

			add_filter( 'enter_title_here', array( $this, 'enter_title_here' ) );
			add_filter( 'post_updated_messages', array( $this, 'updated_messages' ) );

		}

	}

	public function register_post_type () {
		$labels = array(
			'name' => sprintf( _x( '%s', 'post type general name', 'wtg-topquark' ), $this->plural ),
			'singular_name' => sprintf( _x( '%s', 'post type singular name', 'wtg-topquark' ), $this->singular ),
			'add_new' => _x( 'Add New', $this->post_type, 'wtg-topquark' ),
			'add_new_item' => sprintf( __( 'Add New %s', 'wtg-topquark' ), $this->singular ),
			'edit_item' => sprintf( __( 'Edit %s', 'wtg-topquark' ), $this->singular ),
			'new_item' => sprintf( __( 'New %s', 'wtg-topquark' ), $this->singular ),
			'all_items' => sprintf( __( 'All %s', 'wtg-topquark' ), $this->plural ),
			'view_item' => sprintf( __( 'View %s', 'wtg-topquark' ), $this->singular ),
			'search_items' => sprintf( __( 'Search %a', 'wtg-topquark' ), $this->plural ),
			'not_found' => sprintf( __( 'No %s Found', 'wtg-topquark' ), $this->plural ),
			'not_found_in_trash' => sprintf( __( 'No %s Found In Trash', 'wtg-topquark' ), $this->plural ),
			'parent_item_colon' => '',
			'menu_name' => $this->plural,
		);

		$single_slug = apply_filters( 'wtg-topquark_single_slug', _x( sanitize_title_with_dashes( $this->singular ), 'single post url slug', 'wtg-topquark' ) );
		$archive_slug = apply_filters( 'wtg-topquark_archive_slug', _x( sanitize_title_with_dashes( $this->plural ), 'post archive url slug', 'wtg-topquark' ) );

		$defaults = array(
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => false,
			'show_in_menu' => false,
			'query_var' => true,
			'rewrite' => array( 'slug' => $single_slug ),
			'capability_type' => 'post',
			'has_archive' => $archive_slug,
			'hierarchical' => false,
			'supports' => array( 'title' ),
			'menu_position' => 5,
			'menu_icon' => 'dashicons-smiley',
		);

		$args = wp_parse_args( $this->args, $defaults );

		register_post_type( $this->post_type, $args );
	}

	public function updated_messages ( $messages ) {
		global $post, $post_ID;

		$messages[$this->post_type] = array(
			0 => '', // Unused. Messages start at index 1.
			1 => sprintf( __( '%3$s updated. %sView %4$s%s', 'wtg-topquark' ), '<a href="' . esc_url( get_permalink( $post_ID ) ) . '">', '</a>', $this->singular, strtolower( $this->singular ) ),
			2 => __( 'Custom field updated.', 'wtg-topquark' ),
			3 => __( 'Custom field deleted.', 'wtg-topquark' ),
			4 => sprintf( __( '%s updated.', 'wtg-topquark' ), $this->singular ),
			/* translators: %s: date and time of the revision */
			5 => isset($_GET['revision']) ? sprintf( __( '%s restored to revision from %s', 'wtg-topquark' ), $this->singular, wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
			6 => sprintf( __( '%1$s published. %3$sView %2$s%4$s', 'wtg-topquark' ), $this->singular, strtolower( $this->singular ), '<a href="' . esc_url( get_permalink( $post_ID ) ) . '">', '</a>' ),
			7 => sprintf( __( '%s saved.', 'wtg-topquark' ), $this->singular ),
			8 => sprintf( __( '%s submitted. %sPreview %s%s', 'wtg-topquark' ), $this->singular, strtolower( $this->singular ), '<a target="_blank" href="' . esc_url( add_query_arg( 'preview', 'true', get_permalink( $post_ID ) ) ) . '">', '</a>' ),
			9 => sprintf( __( '%s scheduled for: %1$s. %2$sPreview %s%3$s', 'wtg-topquark' ), $this->singular, strtolower( $this->singular ),
			// translators: Publish box date format, see http://php.net/date
			'<strong>' . date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ) . '</strong>', '<a target="_blank" href="' . esc_url( get_permalink($post_ID) ) . '">', '</a>' ),
			10 => sprintf( __( '%s draft updated. %sPreview %s%s', 'wtg-topquark' ), $this->singular, strtolower( $this->singular ), '<a target="_blank" href="' . esc_url( add_query_arg( 'preview', 'true', get_permalink( $post_ID ) ) ) . '">', '</a>' ),
		);

		return $messages;
	}

	public function enter_title_here ( $title ) {
		if ( get_post_type() == $this->post_type ) {
			$title = __( 'Enter the speaker title here', 'wtg-topquark' );
		}
		return $title;
	}

	public function activation () {
		$this->flush_rewrite_rules();
	}

	private function flush_rewrite_rules () {
		$this->register_post_type();
		flush_rewrite_rules();
	}

}